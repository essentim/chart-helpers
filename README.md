[![pipeline status](https://gitlab.com/essentim/chart-helpers/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/essentim/chart-helpers/-/commits/main)

[![Latest Release](https://gitlab.com/essentim/chart-helpers/-/badges/release.svg)](https://gitlab.com/essentim/chart-helpers/-/releases)

![chart-helpers](icon.png)

# Chart Helpers

A library of helper functions for Helm charts.

## How to use

Add the following to your `requirements.yaml` or `Chart.yaml` file:

```yaml
dependencies:
  - name: helpers
    repository: https://gitlab.com/api/v4/projects/53606704/packages/helm/stable
    version: 0
```

## API

### `helpers.name $context`
### `helpers.fullname $context`
### `helpers.chart $context`
### `helpers.labels $context`
### `helpers.selectorLabels $context`
### `helpers.serviceAccountName $context`

### `helpers.servicelabels (list $context $serviceName)`
Generates a set of labels for an application service, such as when your chart has multiple components.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "helpers.fullname" . }}-postgres
  labels:
    {{- include "helpers.serviceLabels" (list . "database") | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "helpers.serviceSelectorLabels" (list . "database") | nindent 6 }}
```

### `helpers.serviceSselectorLabels (list $context $serviceName)`

See: [`helpers.servicelabels`](#helpersservicelabels-list-context-servicename)

### `helpers.registry-credentials-secret (list $context $dockerconfigjson $secretSuffix)`
**See also:** [`helpers.dockerconfigjson`](#helpersdockerconfigjson)

Pass in a list containing:
* the root context
* a map for [`helpers.dockerconfigjson`](#helpersdockerconfigjson) 
* optionally, a name to override the default secret suffix, defaults to `registry-credentials`.

Example:

```yaml
{{- include "helpers.registry-credentials-secret" (list . .Values.registry) -}}
```

```yaml
{{- include "helpers.registry-credentials-secret" (list . .Values.registry "registry-credentials-backend") -}}
```

### `helpers.dockerconfigjson list $context $dockerconfigjson)` 
**See also:** [`helpers.registry-credentials-secret`](#helpersregistry-credentials-secret)

Pass in a map containing: `host`, `username` `password`, `email` to construct a valid _dockerconfigjson_ object.

Example:

```yaml
# values.yaml
registry:
  host: registry.gitlab.com
  username: GITLAB_TOKEN_USERNAME
  password: GITLAB_TOKEN_PASSWORD
  email: ""
```

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "mychart.fullname" . }}-registry-credentials
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: {{ include "helpers.dockerconfigjson" .Values.registry }}
```

### `helpers.waitForPG $postgresConfig`
Generates an init container that waits for a PostgreSQL database to start accepting connections.

Pass in a map containing: `hostname`, `port`, `username`, `password`, `database`.

Example:
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ include "helpers.fullname" . }}-wait-for-pg
  labels:
    {{- include "helpers.labels" . | nindent 4 }}
spec:
  template:
    spec:
      initContainers:
        {{- include "helpers.waitForPG" .Values.postgres | nindent 8 }}
      containers:
        - name: happy
          image: alpine:3.19
          command: [ "/bin/sh", "-c", "exit 0"]
      restartPolicy: Never
```

## Development

This repository has CI that manages the chart version and publishes it to the GitLab package registry. By default it
bumps the patch version, but you can override it by creating a file called `NEXT_VERSION` in the repository root ,
containing a single line - a [SemVer](https://semver.org/) compatible version string.

You are also strongly advised to set pull mode to rebase, as the CD will constantly create commits to the main branch:
```console
git config pull.rebase true
```
