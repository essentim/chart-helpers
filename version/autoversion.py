#!/usr/bin/env python3

##
# This script is used to automatically bump the version of the Helm chart prior to publishing it.
# To explicitly specify the next version, create a file named NEXT_VERSION in the root directory.

import os
import sys
import semver
import ruamel.yaml

# Read the current version from Chart.yaml
with open( 'Chart.yaml', 'r+' ) as chart_file:
    chart_yaml = chart_file.read()

    yaml = ruamel.yaml.YAML()
    yaml.load( chart_yaml )
    chart_data = yaml.load( chart_yaml )
    version = chart_data['version']

    try:
      version = semver.Version.parse( version )
    except ValueError:
      print( "Version in Chart.yaml is not a valid semver version" )
      sys.exit( 1 )

    # Read the next version from NEXT_VERSION if it exists
    try:
        next_version_file = open( 'NEXT_VERSION', 'r' )
        next_version = next_version_file.readline()
        try:
            next_version = semver.Version.parse( next_version )
        except ValueError:
            print( "Version in NEXT_VERSION is not a valid semver version" )
            sys.exit( 1 )
        if next_version <= version:
            print( "The new version must be greater than the current version" )
            sys.exit( 1 )
        os.remove( 'NEXT_VERSION' )
    except FileNotFoundError:
        # If the file is not found, bump the patch version of the current version
        next_version = version.bump_patch()

    # Replace the version in the Chart.yaml
    chart_data['version'] = str( next_version )
    chart_file.seek( 0 ) # Move the cursor to the beginning of the file
    yaml.dump( chart_data, chart_file )
    chart_file.truncate()

    # Print the new version
    print( next_version )
