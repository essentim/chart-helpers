{{- define "helpers.registry-credentials-secret" -}}
{{- $nameSuffix := "registry-credentials" -}}
{{- if ge (len .) 3 -}}
{{- $nameSuffix := index . 2 -}}
{{- end -}}
apiVersion: v1
kind: Secret
type: kubernetes.io/dockerconfigjson
metadata:
  name: {{ include "helpers.fullname" (index . 0) }}-{{ $nameSuffix }}
data:
  .dockerconfigjson: {{ include "helpers.dockerconfigjson" (index . 1) }}
{{- end -}}
