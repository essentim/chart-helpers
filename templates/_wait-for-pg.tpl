{{- define "helpers.waitForPG" -}}
- name: wait-for-pg
  image: alpine:3.19
  command:
    - /bin/sh
    - -c
    - |
      set -e
      apk add postgresql-client
      END_TIME=$(( $(date +%s) + 180 ))
      while [ $(date +%s) -lt $END_TIME ]; do
        pg_isready -h {{ required ".host is required!" .host | quote }} -p {{ required ".port is required!" .port | quote }} -U {{ required ".username is required!" .username | quote }} -d {{ required ".database is required!" .database | quote }} && exit 0
        echo 'Waiting for postgres...'
        sleep 2
      done
      echo 'Error: Timed out waiting for postgres.'
      exit 1
{{- end -}}
