{{/*
Generates a .dockerconfigjson
*/}}
{{- define "helpers.dockerconfigjson" }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .host .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
